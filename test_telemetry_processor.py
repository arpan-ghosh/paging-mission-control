import unittest
from main import TelemetryProcessor
from main import TelemetryStatus


class TestTelemetryProcessor(unittest.TestCase):

    def test_telemetry_status_initialization(self):
        status_data = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"

        telemetry_status = TelemetryStatus(status_data)

        self.assertEqual(telemetry_status.timestamp,
                         "2018-01-01T23:01:05.001Z")
        self.assertEqual(int(telemetry_status.satellite_id), 1001)
        self.assertEqual(telemetry_status.red_high_limit, 101.0)
        self.assertEqual(telemetry_status.yellow_high_limit, 98.0)
        self.assertEqual(telemetry_status.yellow_low_limit, 25.0)
        self.assertEqual(telemetry_status.red_low_limit, 20.0)
        self.assertEqual(telemetry_status.raw_value, 99.9)
        self.assertEqual(telemetry_status.component, "TSTAT")

    def test_telemetry_processor_initialization(self):
        processor = TelemetryProcessor()

        self.assertEqual(processor.results, [])
        self.assertEqual(processor.violations, {})
        self.assertEqual(processor.first_instance, {})

    def test_add_status_and_get_results(self):
        processor = TelemetryProcessor()

        status_data_1 = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"
        status_data_2 = "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT"
        processor.add_status(status_data_1)
        processor.add_status(status_data_2)

        self.assertEqual(len(processor.get_results()), 0)

        # Add status records that trigger violations
        status_data_3 = "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT"
        status_data_4 = "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT"
        status_data_5 = "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT"
        status_data_6 = "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT"
        status_data_7 = "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT"
        status_data_8 = "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT"
        status_data_9 = "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT"
        status_data_10 = "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT"
        status_data_11 = "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT"
        status_data_12 = "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT"
        status_data_13 = "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"

        processor.add_status(status_data_3)
        processor.add_status(status_data_4)
        processor.add_status(status_data_5)
        processor.add_status(status_data_6)
        processor.add_status(status_data_7)
        processor.add_status(status_data_8)
        processor.add_status(status_data_9)
        processor.add_status(status_data_10)
        processor.add_status(status_data_11)
        processor.add_status(status_data_12)
        processor.add_status(status_data_13)

        # Verify that the results contain violations
        results = processor.get_results()
        self.assertEqual(len(results), 2)

        self.assertEqual(int(results[0]["satelliteId"]), 1000)
        self.assertEqual(results[0]["severity"], "RED HIGH")
        self.assertEqual(results[0]["component"], "TSTAT")


if __name__ == '__main__':
    unittest.main()
