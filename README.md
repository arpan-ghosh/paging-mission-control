# Paging Mission Control Solution

## Introduction

The Paging Mission Control alert program is a Python program designed to monitor and process telemetry data from satellite ground operations for an Earth science mission. The program identifies and generates alert messages for specific violation conditions, ensuring that the precision magnetometers on the satellites do not overheat and that power is available to cooling coils.

## Requirements

- Python 3.x
- Input telemetry data in the specified format (ASCII text file)

## How to Use

1. **Input Data**: Prepare a telemetry data file in the specified format. Each line of the file should contain telemetry data with the following fields separated by '|':

```<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>```

Example:
```
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
```

2. **Running the Program**: Open a terminal or command prompt and run the program with the telemetry data file as an argument:
```python main.py telemetry_input.txt```

Replace `telemetry_input.txt` with the path to your telemetry data input file.
To run the unit tests, execute the command  ```python -m unittest test_telemetry_processor.py ``` from the parent directory.


3. **Output**: The program will process the telemetry data and print alert messages to the console in JSON format. The alert messages will include information such as satellite ID, severity, component, and timestamp.


Example Output:

```json
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
```
4. **Functionality**: 

- The program reads telemetry data from the input file, parses it, and processes each telemetry status.
- It identifies violation conditions based on the red high and low limits for battery voltage (BATT) and temperature statistics (TSTAT).
- When a violation condition occurs three times within a five-minute interval for the same satellite and component, an alert message is generated and printed to the console.
- The program uses Python's datetime and json libraries to handle timestamps and JSON formatting.

5. **Solution Notes**:

This particular soluton implementation demonstrates my ability to write a procedural script that doesn't involve anything fancy to get a quick task completed. Throughout the course of my software engineering career there were numerous times I had to write code that was meant for my own consumption- something similar to this coding challenge where we're parsing a file and outputting a JSON formatted object. Unlike my solution for Alphabet Soup, for this particular solution I am not writing a custom data structure, I'm not strcturing the program in numerous classes in a python directory structure, nor am I writing test cases uses pytest. All the code is contained in one file. I wanted to demonstrate my abiity to write-up a "quick-and-easy" script that is still readable, has comments, and works properly, but isn't meant to be "production quality" code in the same way that I wrote the python module solution for Alphabet Soup.
