from datetime import datetime
import json
import sys


class TelemetryStatus:
    def __init__(self, status: str) -> None:
        """
        Initializes the TelemetryStatus object after parsing the status record 
        using the '|' delimiter, and formats the timestamp using datetime.
        """
        telemetry_fields = status.split('|')
        self.timestamp: str = datetime.strptime(
            telemetry_fields[0],
            "%Y%m%d %H:%M:%S.%f").strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
        self.satellite_id: int = telemetry_fields[1]
        self.red_high_limit: float = float(telemetry_fields[2])
        self.yellow_high_limit: float = float(telemetry_fields[3])
        self.yellow_low_limit: float = float(telemetry_fields[4])
        self.red_low_limit: float = float(telemetry_fields[5])
        self.raw_value: float = float(telemetry_fields[6])
        self.component: str = telemetry_fields[7]


class TelemetryProcessor:
    def __init__(self) -> None:
        """
        Initializes the TelemetryProcessor with an empty results list and empty 
        dictionaries for counting violations and storing their first instances.
        """
        self.results = []
        self.violations = {}
        self.first_instance = {}

    def add_status(self, status: str):
        """
        Adds a telemetry status to the processor.

        :param status: A line of telemetry status data.
        """
        telemetry_status = TelemetryStatus(status)
        self.process_status(telemetry_status)

    def process_status(self, status: TelemetryStatus):
        """
        Processes a parsed telemetry status object. It checks for limit violations
        and stores the first instance of each violation. When a violation occurs
        three times it is added to the results.

        :param status: An instance of TelemetryStatus of parsed telemetry data.
        """
        satellite_component_key = (status.satellite_id, status.component)
        if satellite_component_key not in self.violations:
            self.violations[satellite_component_key] = 0
            self.first_instance[satellite_component_key] = None

        # Determine the severity of the violation
        severity = self.determine_severity(
            status.component, status.raw_value,
            status.red_low_limit, status.red_high_limit)

        # Skip if violation count gte 3 or severity not RED LOW or RED HIGH
        if self.violations[satellite_component_key] >= 3 or not severity:
            return

        # Increment the count of violations
        self.violations[satellite_component_key] += 1

        # Store the first instance of a violation
        if self.violations[satellite_component_key] == 1:
            self.first_instance[satellite_component_key] = {
                "satelliteId": status.satellite_id,
                "severity": severity,
                "component": status.component,
                "timestamp": status.timestamp
            }

        # Record the violation in results if it's the third occurrence
        elif self.violations[satellite_component_key] == 3:
            self.results.append(self.first_instance[satellite_component_key])

    def determine_severity(self, component: str, value: float,
                           red_low_limit: float, red_high_limit: float):
        """
        Determines the severity of a violation for one telemetry status record.

        :param component: The component type ("BATT" or "TSTAT").
        :param value: The raw temperature value.
        :param red_low_limit: The lower red limit.
        :param red_high_limit: The upper red limit.
        :return: The severity string ("RED LOW" or "RED HIGH") or None.
        """
        if component == "BATT" and value < red_low_limit:
            return "RED LOW"
        elif component == "TSTAT" and value > red_high_limit:
            return "RED HIGH"
        return None

    def get_results(self):
        """
        Retrieves the list of violations.

        :return: A list of violations
        """
        return self.results


if __name__ == "__main__":
    if len(sys.argv) > 1:
        file_name = sys.argv[1]

    processor = TelemetryProcessor()

    with open(file_name, 'r') as file:
        for line in file:
            processor.add_status(line.strip())

    results = processor.get_results()
    print(json.dumps(results, indent=4))
